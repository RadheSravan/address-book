## Address Book

A simple Spring Boot and Thymeleaf based web application which allows the user to add, view, edit and delete contacts.

##### Tech Stack

* [Spring Boot](https://spring.io/projects/spring-boot) 
* [Gradle](https://gradle.org/)
* [Thymeleaf](https://www.thymeleaf.org/)
* [PostgreSQL](https://www.postgresql.org/)

A walk through on the project setup and code can be found [here](https://technoguy.xyz/2019/10/27/spring-boot-thymeleaf-part-one/")
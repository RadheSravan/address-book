package xyz.technoguy.addressbook.service;

import java.util.List;
import xyz.technoguy.addressbook.persistence.entity.Contact;

public interface AddressBookService {

  public void saveContact(Contact contact) throws Exception;
  
  public List<Contact> getAllContacts() throws Exception;
  
  public Contact getContactById(String id) throws Exception;
  
  public void deleteContact(String id) throws Exception;

}

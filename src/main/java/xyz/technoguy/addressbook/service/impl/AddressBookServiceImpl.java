package xyz.technoguy.addressbook.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.technoguy.addressbook.persistence.entity.Contact;
import xyz.technoguy.addressbook.persistence.repository.ContactRepository;
import xyz.technoguy.addressbook.service.AddressBookService;

@Component
public class AddressBookServiceImpl implements AddressBookService {

  @Autowired
  ContactRepository contactRepository;

  @Override
  public void saveContact(Contact contact) throws Exception {
    contactRepository.save(contact);
  }

  @Override
  public List<Contact> getAllContacts() throws Exception {
    return contactRepository.findAllByOrderByNameAsc();
  }
  
  @Override
  public Contact getContactById(String id) throws Exception {
    return contactRepository.getOne(id);
  }
  
  @Override
  public void deleteContact(String id) throws Exception {
    contactRepository.deleteById(id);
  }

}

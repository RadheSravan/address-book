package xyz.technoguy.addressbook.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import xyz.technoguy.addressbook.persistence.entity.Contact;
import xyz.technoguy.addressbook.service.AddressBookService;

@Controller
public class ViewController {

  @Autowired
  AddressBookService service;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String contacts(Model model) {
    List<Contact> contacts = new ArrayList<>();
    try {
      contacts = service.getAllContacts();
    } catch (Exception e) {

    }
    model.addAttribute("contact", new Contact());
    model.addAttribute("contacts", contacts);
    return "index";
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public ModelAndView addContact(ModelAndView modelAndView,
      @ModelAttribute("contact") Contact contact) {
    List<Contact> contacts = new ArrayList<>();
    try {
      service.saveContact(contact);
      modelAndView.addObject("contact", new Contact());
      modelAndView.addObject("successMessage", "Contact has been successfully added");
    } catch (Exception e) {
      modelAndView.addObject("failureMessage", "Error while adding the contact");
    }
    try {
      contacts = service.getAllContacts();
    } catch (Exception e) {
      modelAndView.addObject("failureMessage", "Error while fetching the contacts");
    }
    modelAndView.addObject("contacts", contacts);
    modelAndView.setViewName("index");
    return modelAndView;
  }

  @RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
  public ModelAndView getContact(ModelAndView modelAndView, @PathVariable String id,
      @ModelAttribute("contact") Contact contact) {
    try {
      contact = service.getContactById(id);
      modelAndView.addObject("contact", contact);
    } catch (Exception e) {
      modelAndView.addObject("failureMessage", "Error while fetching the contact");
    }
    modelAndView.addObject("selectedContact", id);
    modelAndView.setViewName("contact");
    return modelAndView;
  }
  
  @RequestMapping(value = "/contact/{id}", method = RequestMethod.POST,
      params = "action=Go Back")
  public String goBack(Model model, @PathVariable String id,
      @ModelAttribute("contact") Contact contact) {
    return "redirect:/";
  }

  @RequestMapping(value = "/contact/{id}", method = RequestMethod.POST,
      params = "action=Save Details")
  public ModelAndView saveContact(ModelAndView modelAndView, @PathVariable String id,
      @ModelAttribute("contact") Contact contact) {
    try {
      Contact contactToUpdate = service.getContactById(contact.getId());
      if (contactToUpdate.equals(contact)) {
        modelAndView.addObject("successMessage", "There are no changes to save");
      } else {
        contactToUpdate.setName(contact.getName());
        contactToUpdate.setNumber(contact.getNumber());
        contactToUpdate.setAddress(contact.getAddress());
        service.saveContact(contactToUpdate);
        contact = contactToUpdate;
        modelAndView.addObject("successMessage", "Contact details have been successfully updated");
      }
    } catch (Exception e) {
      modelAndView.addObject("failureMessage", "Error while saving the details");
    }
    modelAndView.addObject("selectedContact", contact.getId());
    modelAndView.addObject("contact", contact);
    modelAndView.setViewName("contact");
    return modelAndView;
  }

  @RequestMapping(value = "/contact/{id}", method = RequestMethod.POST,
      params = "action=Delete User")
  public String deleteContact(Model model, @PathVariable String id,
      @ModelAttribute("contact") Contact contact) {
    try {
      service.deleteContact(contact.getId());
      return "redirect:/";
    } catch (Exception e) {
      model.addAttribute("selectedContact", id);
      model.addAttribute("contact", contact);
      model.addAttribute("failureMessage", "Error while deleting the user");
      return "contact";
    }
  }

}

package xyz.technoguy.addressbook.persistence.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.technoguy.addressbook.persistence.entity.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, String> {
  
  public List<Contact> findAllByOrderByNameAsc();
  
}
